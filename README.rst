c2dx
====
Command line tool to generate Cocos2d-X Specific classes.

Installation
------------

First off you need to have Python and Cocos2d-X installed.
There are several ways you can install c2dx generators.

Get Started
^^^^^^^^^^^

..code::
    pip install c2dx

This will install c2dx plus all the dependencies.

Usage
-----

..code::
    c2dx layer -c main



Development setup
-----------------
1. Install python
2. Install pip
3. Install VirtualEnv
4. Go to the Directory you cloned the repo and run ::
    virtualenv .env

..code::
    .env\Scripts\activate
    pip install -r requirements.txt
    pip install --editable .

